Nodelocks provides an additional setting on the node add/edit form allowing an
sufficiently privileged user to "lock" a node, thereby preventing the node's
deletion. This module does not control whether or not a node can be edited.
Typical use case is to prevent unintentional node deletion in cases where
certain nodes serve as more than content, like using a node for a homepage.
