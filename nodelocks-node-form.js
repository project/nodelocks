(function ($) {

  Drupal.behaviors.nodelocksFieldsetSummaries = {
    attach: function (context) {
      $('fieldset.nodelocks-node-settings-form', context).drupalSetSummary(function (context) {
        return Drupal.checkPlain($('input:checked', context).length > 0 ? 'Node is locked' : 'Node is not locked');
      });
    }
  };

})(jQuery);
